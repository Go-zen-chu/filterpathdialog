﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FilterPathDialog {
    public partial class FilterCheckBoxList : UserControl {
        bool hasCountValues = false;

        public FilterCheckBoxList(List<string> itemList) {
            InitializeComponent();
            foreach (var itemStr in itemList)
                itemListBox.Items.Add(new CheckBox() { Content = itemStr });
        }

        public FilterCheckBoxList(List<Tuple<string, int>> itemList) {
            InitializeComponent();
            foreach (var itemTpl in itemList)
                itemListBox.Items.Add(new CheckBox() { Content = string.Format("({0}) {1}", itemTpl.Item2, itemTpl.Item1), IsChecked = true });
            hasCountValues = true;
        }

        private void selectAllCheckBox_Checked(object sender, RoutedEventArgs e) {
            foreach(var cb in itemListBox.Items.Cast<CheckBox>()) cb.IsChecked = true;
        }
        private void selectAllCheckBox_Unchecked(object sender, RoutedEventArgs e) {
            foreach (var cb in itemListBox.Items.Cast<CheckBox>()) cb.IsChecked = false;
        }

        public List<string> GetCheckedItems() {
            if(hasCountValues)
                // Crop counting index string
                return itemListBox.Items.Cast<CheckBox>().Where(cb => cb.IsChecked == true).Select(cb => {
                    var itemStr = cb.Content as string;
                    return itemStr.Remove(0, itemStr.IndexOf(' ', 2) + 1);
                }).ToList();
            else
                return itemListBox.Items.Cast<CheckBox>().Select(cb => cb.Content as string).ToList();
        }

    }
}
