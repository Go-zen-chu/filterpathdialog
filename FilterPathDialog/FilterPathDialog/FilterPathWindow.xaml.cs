﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.Threading.Tasks;

namespace FilterPathDialog {
    /// <summary>
    /// FilteringPathWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class FilterPathWindow : Window {
        string dirPath; // Parent Dir
        List<string[]> splitPathList; // Split pathes found under parent dir
        int maxDepth;
        List<Dictionary<string, int>> matchedPathCountDicts;

        public FilterPathWindow() {
            InitializeComponent();
            this.Width = SystemParameters.PrimaryScreenWidth / 2;
            this.Height = SystemParameters.PrimaryScreenHeight / 2;
            mainGrid.Visibility = System.Windows.Visibility.Collapsed;
            errorGrid.Visibility = System.Windows.Visibility.Visible;
            errorMessageTextBlock.Text = "You need to specify the path to filter by path structure.";
        }

        public FilterPathWindow(string dirPath) {
            InitializeComponent();
            this.Width = SystemParameters.PrimaryScreenWidth / 2;
            this.Height = SystemParameters.PrimaryScreenHeight / 2;
            if (dirPath == null || Directory.Exists(dirPath) == false) {
                mainGrid.Visibility = System.Windows.Visibility.Collapsed;
                errorGrid.Visibility = System.Windows.Visibility.Visible;
                errorMessageTextBlock.Text = "The path isn't correct or dir doen't exist! : " + dirPath;
            } else {
                initialize(dirPath);
            }
        }

        void initialize(string dirPath) {
            mainGrid.Visibility = System.Windows.Visibility.Visible;
            errorGrid.Visibility = System.Windows.Visibility.Collapsed;
            this.dirPath = dirPath;
            this.Title += " : " + dirPath;
            this.splitPathList = new List<string[]>();
            this.maxDepth = 0;
            this.matchedPathCountDicts = new List<Dictionary<string, int>>();

            Task.Factory.StartNew(() => {
                int fileCount = 0;
                foreach (var csvPath in Directory.EnumerateFiles(dirPath, "*.csv", SearchOption.AllDirectories)) {
                    if (Path.GetFileName(csvPath) != "errata.csv" && Path.GetFileName(csvPath) != "averageErrorDiff.csv") continue;
                    var relativePath = csvPath.Remove(0, dirPath.Length);
                    if (relativePath[0] == Path.DirectorySeparatorChar) relativePath = relativePath.Remove(0, 1); // if exists, remove the path separator on the head 
                    var splitPath = relativePath.Split(Path.DirectorySeparatorChar);
                    splitPathList.Add(splitPath);
                    var newDepth = splitPath.Length - 1; // just count the dirs depth
                    if (maxDepth < newDepth) {
                        // add count dicts as many as depth increased
                        foreach (var i in Enumerable.Range(1, newDepth - maxDepth)) matchedPathCountDicts.Add(new Dictionary<string, int>());
                        maxDepth = newDepth;
                    }
                    for (int d = 0; d < newDepth; d++) {
                        var key = splitPath[d];
                        matchedPathCountDicts[d][key] = matchedPathCountDicts[d].ContainsKey(key) ? matchedPathCountDicts[d][key] + 1 : 1;
                    }
                    fileCount++;
                    if(fileCount%10 == 0)
                        filterGroupBox.Dispatcher.BeginInvoke(new Action(() => {
                            filterGroupBox.Header = string.Format("Filter : counting {0}", fileCount);
                        }));
                }
                this.Dispatcher.BeginInvoke(new Action(() => {
                    constructFilters();
                    filterButton.IsEnabled = true;
                }));
            });
        }

        void constructFilters()
        {
            if (maxDepth != matchedPathCountDicts.Count) return;
            for (int depth = 0; depth < maxDepth; depth++)
            {
                filtersGrid.ColumnDefinitions.Add(new ColumnDefinition());
                var filterGrid = new FilterCheckBoxList(matchedPathCountDicts[depth].Select(kv => new Tuple<string, int>(kv.Key, kv.Value)).ToList());
                Grid.SetColumn(filterGrid, depth);
                filtersGrid.Children.Add(filterGrid);
            }
        }

        void filterButton_Click(object sender, RoutedEventArgs e)
        {
            if(splitPathList == null || splitPathList.Count == 0) return;
            var checkedFiltersList = filtersGrid.Children.Cast<FilterCheckBoxList>().Select(fg => fg.GetCheckedItems()).ToList();
            Task.Factory.StartNew(() => {
                foreach (var splitPath in splitPathList) {
                    string csvStr = null;
                    for (int d = 0; d < checkedFiltersList.Count; d++) {
                        if (checkedFiltersList[d].Contains(splitPath[d])) {
                            if (d == checkedFiltersList.Count - 1) {
                                csvStr = string.Join(",", splitPath) + "," + getResults(Path.Combine(dirPath, string.Join(Path.DirectorySeparatorChar.ToString(), splitPath)));
                            }
                        } else {
                            break;
                        }
                    }
                    if (csvStr != null) {
                        resultsTextBox.Dispatcher.BeginInvoke(new Action(() => {
                            var oldText = resultsTextBox.Text;
                            var newText = oldText == "" ? csvStr : string.Join(Environment.NewLine, oldText, csvStr);
                            resultsTextBox.Text = newText;
                        }));
                    }
                }
            });
        }

        string getResults(string filePath) {
            var fileName = Path.GetFileName(filePath);
            if (fileName == "errata.csv")
                using (var sr = new StreamReader(filePath)) {
                    string line = null;
                    foreach (var lineNum in Enumerable.Range(1, 4)) line = sr.ReadLine();
                    return line;
                } else if (fileName == "averageErrorDiff.csv")
                using (var sr = new StreamReader(filePath)) {
                    string line = null;
                    foreach (var lineNum in Enumerable.Range(1, 4)) line = sr.ReadLine();
                    return line.Split(',')[1];
                } else
                throw new Exception();
        }

        public string[] GetFilteredPathes() {
            var pathesText = resultsTextBox.Text;
            if (pathesText != null || pathesText == "")
                return null;
            else
                return resultsTextBox.Text.Split(Environment.NewLine.ToCharArray());
        }

        #region Error Grid
        private void newDirPathButton_Click(object sender, RoutedEventArgs e) {
            var newDirPath = SelectDirPath(null, "Select Directory you wants to filter");
            if (newDirPath != null) initialize(newDirPath);
        }

        private void quitButton_Click(object sender, RoutedEventArgs e) {
            this.Close();
        }
        #endregion

        static string SelectDirPath(string rootPath = null, string description = "Select Directory") {
            var fbd = new System.Windows.Forms.FolderBrowserDialog();
            fbd.SelectedPath = rootPath;
            fbd.Description = description;
            var dialogResult = fbd.ShowDialog();
            if (dialogResult == System.Windows.Forms.DialogResult.OK) {
                return fbd.SelectedPath;
            }
            return null;
        }
    }
}
